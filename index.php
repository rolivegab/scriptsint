<?php	
	require_once(__DIR__.'/Core/Core.php');

	if (isset($_GET['command'])) {
		$core = new Core();
		Core::initialize();
		$cmd = Core::CMD();

		if ($_GET['command'] == 'searchCourses') {
			$r = $cmd::searchCourses(['TURMA' => $_GET['turma']]);
			Core::echoConsole($r);
		} else if ($_GET['command'] == 'listStudentsOfCourse') {
			echo '<textarea>';
			$cmd::listStudentsOfCourse($_GET['coddisciplina'], $_GET['turma'], $_GET['curso']);
			echo '</textarea>';
		} else if ($_GET['command'] == 'searchStudentClasses') {
			$r = $cmd::searchStudentClasses($_GET['matricula']);
			Core::echoConsole($r);
		} else if ($_GET['command'] == 'searchMatricula') {
			$r = $cmd::searchMatricula($_GET['studentname']);
			Core::echoConsole($r);
		} else if ($_GET['command'] == 'searchStudent') {
			$r = $cmd::searchStudent($_GET['matricula']);
			Core::echoConsole($r);
		}
	}
?>

<form type="GET" action="">
	Pesquisar cursos:<br>
	<input type="hidden" name="command" value="searchCourses">
	<input type="text" name="turma" placeholder="turma"><input type="submit">
</form>
<form type="GET" action="">
	Gerar lista de estudantes para ser enviada ao Moodle:<br>
	<input type="hidden" name="command" value="listStudentsOfCourse">
	<input type="text" name="coddisciplina" placeholder="coddisciplina">
	<input type="text" name="turma" placeholder="turma">
	<input type="text" name="curso" placeholder="nome curto do curso">
	<input type="submit">
</form>
<form type="GET" action="">
	Pesquisar turmas cadastradas para o aluno:<br>
	<input type="hidden" name="command" value="searchStudentClasses">
	<input type="text" name="matricula" placeholder="matricula">
	<input type="submit">
</form>
<form type="GET" action="">
	Pesquisar matrícula do aluno:<br>
	<input type="hidden" name="command" value="searchMatricula">
	<input type="text" name="studentname" placeholder="student name">
	<input type="submit">
</form>
<form type="GET" action="">
	Pesquisar aluno:<br>
	<input type="hidden" name="command" value="searchStudent">
	<input type="text" name="matricula" placeholder="matricula">
	<input type="submit">
</form>