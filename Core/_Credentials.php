<?php

    class _Credentials
    {
        public function setDB($dblib, $dbhost, $dbport, $dbname, $dbuser, $dbpassword)
        {
            self::$dblib = $dblib;
            self::$dbhost = $dbhost;
            self::$dbport = $dbport;
            self::$dbname = $dbname;
            self::$dbuser = $dbuser;
            self::$dbpassword = $dbpassword;
        }

        public function setWS($token)
        {
            self::$token = $token;
        }

        public static function DB()
        {
            return(object)array(
                    'dblib' => self::$dblib,
                    'dbhost' => self::$dbhost,
                    'dbport' => self::$dbport,
                    'dbname' => self::$dbname,
                    'dbuser' => self::$dbuser,
                    'dbpassword' => self::$dbpassword
            );
        }

        public static function WS()
        {
            $obj = new stdClass();
            $obj->token = self::$token;

            return $obj;
        }
        
        public static $dblib;
        public static $dbhost;
        public static $dbport;
        public static $dbname;
        public static $dbuser;
        public static $dbpassword;
        public static $token;
    }
