<?php
	require_once( realpath(__DIR__.'/../config.php' ) );
	require_once(__DIR__.'/Log.php');
	require_once(__DIR__.'/ConnDB.php');
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncCategories.php') );
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncCourses.php') );	
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncStudents.php') );
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncStudentEnrolments.php') );
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncTeacherEnrolments.php') );
	require_once( realpath(__DIR__.'/../Imp/CDB/SyncTeachers.php') );
	require_once( realpath(__DIR__.'/../Imp/CDB/Data.php') );
	require_once( realpath(__DIR__.'/../Imp/SyncUser.php' ) );
	require_once( realpath(__DIR__.'/../Imp/SyncStudents.php') );
	require_once( realpath(__DIR__.'/../Imp/SyncTeachers.php') );
	require_once( realpath(__DIR__.'/../Imp/SyncCategories.php') );
	require_once( realpath(__DIR__.'/../Imp/SyncCourses.php' ) );
	require_once( realpath(__DIR__.'/../Imp/SyncStudentEnrolments.php' ) );
	require_once( realpath(__DIR__.'/../Imp/SyncTeacherEnrolments.php' ) );
	require_once( realpath(__DIR__.'/../Imp/Commands.php' ) );
	require_once( realpath(__DIR__.'/../Imp/Data.php' ) );
	require_once( realpath(__DIR__.'/../Imp/WSManager.php') );

	class Core {
		/**
		 * Configura para que o script não tenha tempo máximo de execução.
		 */
		public static function initialize() {
			// Config:
			global $config;
			static::$config = $config->ava;

			// Commands:
			static::$commands = new Commands(static::$data, static::$config);
		}

		public static function automaticSync() {

			// Initialize classes:
			SyncCategories::initialize();
			SyncCourses::initialize();
			SyncStudents::initialize();
			SyncStudentEnrolments::initialize();
			SyncTeacherEnrolments::initialize();

			SyncCategories::run(2018, 1);
			SyncCourses::run(2018, 1);
			SyncStudents::run(2018, 1);
			SyncTeachers::run(2018, 1);
			SyncStudentEnrolments::run(2018, 1);
			SyncTeacherEnrolments::run(2018, 1);

			// // Categories:
			// static::$syncCategories = new SyncCategories(static::$config->disciplina, static::$data);
			// static::$syncCategories->getData();
			// static::$syncCategories->checkData();
			// static::$syncCategories->syncCategories();

			// // Subjects:
			// static::$syncCourses = new syncCourses(static::$config->disciplina, static::$data);
			// static::$syncCourses->getData();
			// static::$syncCourses->checkData();
			// static::$syncCourses->syncCourses();

			// // Student Enrolments:
			// static::$syncStudentEnrolments = new SyncStudentEnrolments($config->views->papelaluno, $this->data);
			// static::$syncStudentEnrolments->getData();
			// static::$syncStudentEnrolments->syncStudentEnrolments();

			// // Teacher Enrolments:
			// static::$syncTeacherEnrolments = new SyncTeacherEnrolments($config->views->papelprofessor, $this->data);
			// static::$syncTeacherEnrolments->getData();
			// static::$syncTeacherEnrolments->syncTeacherEnrolments();
		}

		public static function CMD() {
			return static::$commands;
		}

		/**
		 * Imprime no console do navegador, aceita vários parâmetros.
		 */
		public static function echoConsole() : void {
			$args = func_get_args();
			echo '<script>console.log(';
			foreach ($args as $key=>$arg) {
				$aux = json_encode($arg);
				if (json_last_error() !== JSON_ERROR_NONE) {
					echo json_encode('JSON Err: '.json_last_error_msg().'. at: '.__FILE__.' '.__LINE__);
				}

				if ($key == 0) {
					echo $aux;
				} else {
					echo ', '.json_encode($arg);
				}
			}
			echo ');</script>';
		}

		private static $syncStudents;
		private static $syncTeachers;
		private static $syncCategories;
		private static $syncCourses;
		private static $syncStudentEnrolments;
		private static $data;
		private static $config;
		private static $commands;
	}