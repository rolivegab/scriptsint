<?php
    require_once( realpath(__DIR__.'/../config.php'));
    require_once(__DIR__.'/Log.php');
    class ConnDB {
        public function __construct($config) {
            $this->dblib = $config->dblib;
            $this->dbhost = $config->dbhost;
            $this->dbport = $config->dbport;
            $this->dbname = $config->dbname;
            $this->dbuser = $config->dbuser;
            $this->dbpassword = $config->dbpassword;
            $this->dbtimeout = $config->dbtimeout;
            $this->log = new Log('ConnDB');
            $this->Connect();
        }

        public function Connect() {
            $opt = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                PDO::ATTR_TIMEOUT => $this->dbtimeout
            );

            try {
                if ($this->dblib == 'dblib') {
                    $this->dbconn = new PDO("$this->dblib:version=7.0;charset=UTF-8;host=$this->dbhost:$this->dbport;dbname=$this->dbname", "$this->dbuser", "$this->dbpassword", $opt);
                } else if ($this->dblib == 'mysql') {
                    $this->dbconn = new PDO("$this->dblib:host=$this->dbhost:$this->dbport;dbname=$this->dbname;charset=utf8", "$this->dbuser", "$this->dbpassword", $opt);
                } else {
                    echo 'fu';die();
                }
            } catch (Exception $e) {
                $this->log->putL($e->getMessage());
                die();
            }

            return true;
        }

        protected function db(): PDO {
            return $this->dbconn;
        }

        protected function prepare($sql, $params = NULL): PDOStatement {   
            $stmt = $this->dbconn->prepare($sql);
            if($params) {
                foreach($params as $key=>$param) {
                    if(is_int($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_INT);
                    } else if (is_string($param)) {
                        $stmt->bindValue($key, $param, PDO::PARAM_STR);
                    }
                }
            }

            return $stmt;
        }

        private $dbconn;
        private $result;
        private $log;
    }
