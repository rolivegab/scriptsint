<?php
	require_once( realpath(__DIR__.'/../config.php'));

	class Log {
		/**	
		* Recebe um nome e cria o arquivo para poder registrar os logs.
		*/
		function __construct($file) {
			global $config;
			static::$group = $config->apacheuser;

			if (!static::$isInitialized) {
				static::$rootdir = realpath(__DIR__.'/../');

				if (!static::checkPermissions(static::$rootdir)) {
					die();
				}

				static::$day = date('d');
				static::$month = date('m');
				static::$year = date('Y');

				static::$isInitialized = true;
			}

			$this->path = static::$rootdir.'/logs/'.static::$year.'/'.static::$month.'/'.static::$day.'/'.$file.'.txt';
			$this->headerAlreadyPrinted = false;

			$this->checkDirectory();
		}

		/**
		* Verifica se o diretório em específico possui permissões de escrita.
		*/
		private static function checkPermissions(string $rootdir) : bool {
			if (!is_writable($rootdir)) {
				echo "O diretório $rootdir não tem permissões de escrita para a criação dos logs, favor resolver.";
				return false;
			}

			return true;
		}

		public function createDay() {
			$dir = static::$rootdir.'/logs/'.static::$year.'/'.static::$month.'/'.static::$day;
			if (mkdir($dir)) {
				chmod($dir, 0770);
				chgrp($dir, static::$group);
			} else {
				echo 'Diretório dia não criado.';
			}
		}

		public function createMonth() {
			$dir = static::$rootdir.'/logs/'.static::$year.'/'.static::$month;
			if (mkdir($dir)) {
				chmod($dir, 0770);
				if (chgrp($dir, static::$group)) {
					$this->createDay();
				}
			} else {
				echo 'Diretório mês não criado.';
			}
		}

		public function createYear()  {
			$dir = static::$rootdir.'/logs/'.static::$year;
			if (mkdir($dir)) {
				chmod($dir, 0770);
				if (chgrp($dir, static::$group)) {
					$this->createMonth();
				}
			} else {
				echo 'Diretório ano não criado.';
			}
		}

		public function createLog() {
			$dir = static::$rootdir.'/logs';
			if (mkdir($dir)) {
				chmod($dir, 0770);
				if (chgrp($dir, static::$group)) {
					$this->createYear();
				}
			} else {
				echo 'Diretório log não criado.';
			}
		}

		public function checkDirectory() {
			if (!is_dir(static::$rootdir.'/logs')) {
				$this->createLog();
			} else if (!is_dir(static::$rootdir.'/logs/'.static::$year)) {
				$this->createYear();
			} else if (!is_dir(static::$rootdir.'/logs/'.static::$year.'/'.static::$month)) {
				$this->createMonth();
			} else if (!is_dir(static::$rootdir.'/logs/'.static::$year.'/'.static::$month.'/'.static::$day)) {
				$this->createDay();
			}
		}

		public function setState($var) {
			if ($var == 'syncStudents') {
				$this->state = $var;
				$this->isStateSelected = true;
			} else {
				echo 'Estado não existente';
			}
		}

		public function put($text) {
			if (file_exists($this->path)) {
				if (!$this->headerAlreadyPrinted) {
					$this->headerAlreadyPrinted = true;
					$this->header();
				}

				file_put_contents($this->path, $text, FILE_APPEND);
			} else {
				file_put_contents($this->path, $text, FILE_APPEND);
				chgrp($this->path, static::$group);
				chmod($this->path, 0770);
			}
		}

		public function putL($text) {
			$this->put($text.PHP_EOL);	
		}

		private function header() {
			$this->put('====== '.date('d-m-Y H:i:s').' ======'.PHP_EOL);
		}

		public function close() {
			$this->put('#'.PHP_EOL);
		}

		private $path;
		private static $group;
		private static $isInitialized;
		private static $rootdir;
		private static $month;
		private static $day;
		private static $year;
	}