<?php
    class SM
    {
        public static function StartSession()
        {
            $status = session_status();
            if ($status == PHP_SESSION_NONE) {
                session_start();
            } elseif ($status == PHP_SESSION_DISABLED) {
                // FAZER!
            }
            
            if (!isset($_SESSION['STARTED'])) {
                $_SESSION['STARTED'] = time();
            }
            $_SESSION['LAST_ACTIVITY'] = time();
        }
        
        public static function isPOST($var)
        {
            return isset($_POST[$var]);
        }
        
        public static function isGET($var)
        {
            return isset($_GET[$var]);
        }
        
        public static function isSESSION($var)
        {
            SM::StartSession();
            
            if (isset($_SESSION[$var])) {
                return true;
            } else {
                return false;
            }
        }
        public static function getGET($var)
        {
            return $_GET[$var];
        }
        
        public static function getPOST($var)
        {
            return $_POST[$var];
        }

        public static function getSESSION($var)
        {
            SM::StartSession();
            
            return $_SESSION[$var];
        }

        public static function setGET($var, $value)
        {
            $_GET[$var] = $value;
        }

        public static function setPOST($var, $value)
        {
            $_POST[$var] = $value;
        }

        public static function setSESSION($var, $valor)
        {
            SM::StartSession();
            
            $_SESSION[$var] = $valor;
        }

        public static function unsetGET($var)
        {
            unset($_GET[$var]);
        }

        public static function unsetPOST($var)
        {
            unset($_POST[$var]);
        }

        public static function CloseSESSION()
        {
            SM::StartSession();
            
            session_unset();
            session_destroy();
        }
        
        public static function unsetSESSION($var)
        {
            SM::StartSession();
            
            unset($_SESSION[$var]);
        }

        public static function echoSESSIONStatus()
        {
            SM::StartSession();

            $result = "";

            $result .= '<pre>';
            $result .= 'last activity: ';
            
            if (isset($_SESSION['LAST_ACTIVITY'])) {
                $result .= $_SESSION['LAST_ACTIVITY']->format('d/m/y h:i:s');
            } else {
                $result .= 'didn\'t set';
            }
            $result .= '<br>';
            
            $result .= 'started: ';
            if (isset($_SESSION['STARTED'])) {
                $result .= $_SESSION['STARTED']->format('d/m/y h:i:s');
            } else {
                $result .= 'didn\'t set';
            }
            $result .= '<br>';
            
            $result .= 'username: ';
            if (isset($_SESSION['username'])) {
                $result .= $_SESSION['username'];
            } else {
                $result .= 'didn\'t set';
            }
            $result .= '<br>';
            
            $result .= 'password: ';
            if (isset($_SESSION['password'])) {
                $result .= $_SESSION['password'];
            } else {
                $result .= 'didn\'t set';
            }
            $result .= '<br>';
            $result .= '</pre>';

            return $result;
        }
    }