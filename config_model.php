<?php
	/**
	 * Este módulo funciona em cima de 5 tabelas:
	 * 1 - Aluno
	 * 2 - Papel Aluno
	 * 3 - Professor
	 * 4 - Papel Professor
	 * 5 - Disciplinas
	 * 
	 * Para alterar o nome das views, basta alterar o nome das variáveis abaixo:
	 */

	date_default_timezone_set('America/Recife');
	
	// Configura o tempo limite de conexão total para ilimitado:
	set_time_limit(0);

	global $config;

	$config = new stdClass();
	$config->ead = new stdClass();
	$config->ava = new stdClass();
	$config->WS = new stdClass();

	$config->apacheuser = 'www-data';
	$config->maxTries = 3;
	$config->sleepTime = 3000;

	// $config->minYear = '2016.1';
	// $config->maxYear = 'max';

	// Token from Universus Web Service User to Universus Web Service Service:
	$config->WS->token = '';

	// Moodle url, não colocar '/' no fim:
	$config->WS->moodleurl = '';

	// Configurações do Universus:
	$config->universus = new stdClass();
	$config->universus->dblib = '';
	$config->universus->dbhost = '';
	$config->universus->dbport = '';
	$config->universus->dbname = '';
	$config->universus->dbuser = '';
	$config->universus->dbpassword = '';
	$config->universus->dbtimeout = 60;

	// Configurações do Moodle:
	$config->moodle = new stdClass();
	$config->moodle->dblib = '';
	$config->moodle->dbhost = '';
	$config->moodle->dbport = '';
	$config->moodle->dbname = '';
	$config->moodle->dbuser = '';
	$config->moodle->dbpassword = '';
	$config->moodle->dbtimeout = 30;