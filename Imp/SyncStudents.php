<?php
class SyncStudents extends SyncUser
{
	function initialize()
	{
		static::$log = new Log('syncStudents');
	}
	/**
	 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
	 * @param	string	$view	Nome da view a qual a classe irá se conectar.
	 * @param	Data	$data	Armazena dados que serão reutilizados depois.
	 */
	function run($ano, $periodo)
	{
		static::getData($ano, $periodo);
	}

	/**
	 * Pega os dados necessários para a execução da sincronização.
	 */
	function getData($ano, $periodo)
	{
		// Pegando alunos do Universus.
		$udb = new CDB\SyncStudents();
		$datadb = new CDB\Data();
		$stmt = $udb->_universusStudents($ano, $periodo);

		if (!$stmt) {
			static::$log->putL('Erro ao pegar os estudantes no banco.');
			die();
		}

		static::$universusStudents = $stmt->fetchAll();
		static::$moodleStudents = Data::getMoodleUsers();
		// Core::echoConsole('Universus Students', static::$universusStudents);
		// Core::echoConsole('Moodle Students', static::$moodleStudents);
		static::checkData();
	}

	/**
	 * Checa se existe algum campo incorreto, ou duplicado:
	 */
	public static function checkData()
	{
		$s = &static::$universusStudents;
		$l = static::$log;

		$size = count($s);
		$erros = 0;

		for ($i = 0; $i < $size; $i++) {
			// Converte username para minúsculo
			$s[$i]->matricula = strtolower($s[$i]->matricula);

			// Checa se o username está no formato correto
			// ENVIAR EMAIL DEPOIS!
			if (!static::checkUsername($s[$i]->matricula, 'az', '09', '_', '-', '.', '@')) {
				$l->putL('Usuário \'' . $s[$i]->matricula . '\' ignorado por conter caracteres inválidos no campo de usuário.');
				array_splice($s, $i, 1);
				$size--;
				$i--;
				$erros++;
				continue;
			}

			// Checa se o email está no formato correto
			// ENVIAR EMAIL DEPOIS!
			if (!filter_var($s[$i]->email, FILTER_VALIDATE_EMAIL)) {
				$l->putL('Usuário \'' . $s[$i]->matricula . '\' ignorado por conter caracteres inválidos no campo de email. Email: \'' . $s[$i]->email . '\'.' . PHP_EOL);
				array_splice($s, $i, 1);
				$size--;
				$i--;
				$erros++;
				continue;
			}

			// Checa se há campos únicos duplicados:
			for ($j = $i + 1; $j < $size; $j++) {
				// ENVIAR EMAIL PARA O SEGUNDO USUÁRIO!
				if ($s[$i]->matricula == $s[$j]->matricula) {
					$l->putL('Usuário duplicado: \'' . $s[$i]->matricula . '\'.');
					$l->putL('Apenas o primeiro será adicionado.');
					array_splice($s, $j, 1);
					$size--;
					$j--;
					$erros++;
					continue;
				}

				// ENVIAR EMAIL DEPOIS PARA O SEGUNDO USUÁRIO!
				if (strtolower($s[$i]->email) == strtolower($s[$j]->email)) {
					$l->putL('Email duplicado entre usuários: \'' . $s[$i]->matricula . '\' e \'' . $s[$j]->matricula . '\'. Email: \'' . $s[$i]->email . '\'. ');
					$l->putL('Apenas o usuário \'' . $s[$i]->matricula . '\' será adicionado.');
					array_splice($s, $j, 1);
					$size--;
					$j--;
					$erros++;
					continue;
				}
			}

			//
		}

		$l->putL('Usuários do universus com ERRO: ' . $erros);
		static::synchronize();
	}

	/**
	 * Faz a sincronização de estudantes do Universus com o Moodle.
	 */
	public static function synchronize()
	{
		global $config;
		$maxTries = $config->maxTries;
		$sleepTime = $config->sleepTime;
		$userstobeinserted = [];
		$usersreallyinserted = [];
		$ignoredusers = 0;

		// Selecionando quais usuários devem ser erros
		foreach (static::$universusStudents as $student) {
			$flag = 0;
			for ($i = 0; $i < static::$moodleStudents->size; $i++) {
				// Não insere alunos do universus no moodle cuja matricula (ou email), já exista no moodle).
				if (strtolower($student->matricula) == strtolower(static::$moodleStudents->username[$i]) || strtolower($student->email) == strtolower(static::$moodleStudents->email[$i])) {
					$ignoredusers++;
					$flag = 1;
					break;
				}
			}

			if ($flag == 0) {
				$userstobeinserted[] = $student;
			}
		}

		$size = count($userstobeinserted);

		static::$log->putL('Alunos que serão inseridos: ' . $size);
		static::$log->putL('Alunos saltados: ' . $ignoredusers);

		// Insere os alunos no moodle de 50 em 50
		$aux = [];
		$offset = 0;
		foreach ($userstobeinserted as $key => $user) {
			$aux[] = $user;
			if ($offset == 49 || $key == $size - 1) {
				for ($i = 0; $i < $maxTries; $i++) {
					$create_users_resp = WSManager::create_users($aux, 'student');
					if ($create_users_resp) {
						foreach ($create_users_resp as $resp) {
							$usersreallyinserted[] = $resp;
						}

						break;
					} else {
						static::$log->putL('Falha ao executar create_users, tentativa ' . $i);
						usleep($sleepTime);
					}
				}

				$aux = [];
				$offset = 0;
			} else {
				$offset++;
			}
		}

		static::$log->putL('Número de usuários realmente inseridos: ' . count($usersreallyinserted));
		static::$log->putL('IDs inseridos: ');
		foreach ($usersreallyinserted as $user) {
			static::$log->putL($user->id . ' ' . $user->username);
		}
	}


	/**
	 * Gera uma lista de estudantes que pode ser utilizada para popular o Moodle através 
	 * da função core_user_create_users do webservice do Moodle.
	 */
	public function addRandomStudents(int $num) : void
	{
		for ($i = 0; $i < $num; $i++) {
			static::$universusStudents[] = array(
				'ALUNO' => 'random',
				'SOBRENOME' => lib::random_str(50),
				'MATRICULA' => lib::random_str(20, '1234567890'),
				'CPF' => lib::random_str(11),
				'EMAIL' => lib::random_str(15) . '@' . lib::random_str(4) . '.com'
			);
		}
	}

	private static $log;
	private static $view;
	private static $universusStudents;
	private static $moodleStudents;
}