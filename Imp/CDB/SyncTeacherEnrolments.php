<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;
    use PDO;

    class SyncTeacherEnrolments extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusTeacherEnrolments($ano, $periodo, $from, $space): PDOStatement {
            global $config;
            $view = $config->ead->papelprofessor;

            $sql = ';WITH Results_CTE AS
				(
					SELECT
					codinterno_disc,
					login,
					turma,
					ROW_NUMBER() OVER (ORDER BY LOGIN, TURMA, CODINTERNO_DISC) AS RowNum
					FROM '.file_get_contents(__DIR__.'/sql/PapeisProfessor.sql').'
				)
				SELECT * FROM Results_CTE WHERE RowNum >= '.$from.'
				AND RowNum < '.$from + $space;

            $stmt = $this->prepare($sql, ['ano' => $ano, 'periodo' => $periodo]);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }