<?php
    namespace CDB;
    require_once( realpath(__DIR__.'/../../Core/ConnDB.php'));
    use ConnDB;
    use PDOStatement;

    class Views extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function getViews() {
            $sql = "SELECT name FROM sys.views WHERE name LIKE '%_EAD'";
            $stmt = $this->prepare($sql);
            if ($stmt->execute()) {
                return $stmt->fetchAll();
            }
        }

        function showViewInfo($viewName) {
            $sql = 
            "SELECT 
                definition
            FROM sys.objects o 
            JOIN sys.sql_modules m ON (m.object_id = o.object_id) 
            WHERE 
                o.object_id = object_id(:viewName) 
                AND o.type = 'V'";
            $stmt = $this->prepare($sql, ['viewName' => $viewName]);
            if ($stmt->execute() && $aux = $stmt->fetch()) {
                return $aux->definition;
            }
        }
    }