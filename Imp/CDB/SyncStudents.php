<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;

    class SyncStudents extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusStudents($ano, $periodo): PDOStatement {
            global $config;
            $sql = 'SELECT aluno, email, matricula, sobrenome FROM ('.file_get_contents(__DIR__.'/sql/ListaAluno.sql').') as v1';
            $stmt = $this->prepare($sql, ['ano' => $ano, 'periodo' => $periodo]);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }