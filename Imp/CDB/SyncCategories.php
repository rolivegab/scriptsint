<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;

    class SyncCategories extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusCategories($ano, $periodo): PDOStatement {
            global $config;
            $sql = 'SELECT curso FROM ('.file_get_contents(__DIR__.'/sql/ListaDisciplina.sql').') as v1';
            $stmt = $this->prepare($sql, ['ano' => $ano, 'periodo' => $periodo]);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }