<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;

    class Data extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->moodle);
        }

        function _moodleUsers(): PDOStatement {
            $sql = "SELECT id, username, email FROM mdl_user WHERE deleted = 0";
            $stmt = $this->prepare($sql);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }

        function _moodleCategories(): PDOStatement {
            $sql = "SELECT id, name FROM mdl_course_categories";
            $stmt = $this->prepare($sql);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }

        function _moodleCourses(): PDOStatement {
            $sql = "SELECT id, shortname, idnumber FROM mdl_course";
            $stmt = $this->prepare($sql);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }

        function _moodleUserEnrolments($roleid): PDOStatement {
            $sql = "SELECT
            mdl_user.id AS userid, 
            mdl_user.username, 
            mdl_course.id AS courseid,
            mdl_course.shortname 
            FROM mdl_role_assignments 
            LEFT JOIN mdl_context ON mdl_context.id = mdl_role_assignments.contextid
            LEFT JOIN mdl_user ON mdl_user.id = mdl_role_assignments.userid
            LEFT JOIN mdl_course ON mdl_course.id = mdl_context.instanceid
            WHERE mdl_context.contextlevel = 50
            AND mdl_role_assignments.roleid = :roleid";

            $stmt = $this->prepare($sql, ['roleid' => $roleid]);

            if(!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }