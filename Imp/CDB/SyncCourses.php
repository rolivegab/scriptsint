<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;

    class SyncCourses extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusCourses($ano, $periodo): PDOStatement {
            global $config;
            $sql = 'SELECT disciplina, curso, coddisciplina, turma FROM ('.file_get_contents(__DIR__.'/sql/ListaDisciplina.sql').') as v1';
            $stmt = $this->prepare($sql, ['ano' => $ano, 'periodo' => $periodo]);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }