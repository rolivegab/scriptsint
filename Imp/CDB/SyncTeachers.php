<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;

    class SyncTeachers extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusTeachers(): PDOStatement {
            global $config;
            $sql = 'SELECT login, email, nome, sobrenome FROM ('.file_get_contents(__DIR__.'/sql/ListaProfessor.sql').') as v1';
            $stmt = $this->prepare($sql);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }