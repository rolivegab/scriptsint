<?php
    namespace CDB;
    use ConnDB;
    use PDOStatement;
    use PDO;

    class SyncStudentEnrolments extends ConnDB {
        function __construct() {
            global $config;
            parent::__construct($config->universus);
        }

        function _universusStudentEnrolments($ano, $periodo, $from, $space): PDOStatement {
            global $config;

            $sql = ';WITH Results_CTE AS
            (
                SELECT 
                coddisciplina, 
                matricula, 
                turma,
                ROW_NUMBER() OVER (ORDER BY MATRICULA, TURMA, CODDISCIPLINA) AS RowNum
                FROM ('.file_get_contents(__DIR__.'/sql/PapeisAluno.sql').') as v1
            )
            SELECT * FROM Results_CTE WHERE RowNum >= '.$from.'
            AND RowNum < '.($from + $space);

            $stmt = $this->prepare($sql, ['ano' => $ano, 'periodo' => $periodo]);
            if (!$stmt->execute()) {
                return NULL;
            }

            return $stmt;
        }
    }