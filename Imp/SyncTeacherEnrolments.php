<?php
	abstract class SyncTeacherEnrolments extends Sync {
		function initialize() {
			static::$log = new Log('SyncTeacherEnrolments');
		}
		/**
		 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
		 * @param	string	$view	Nome da view a qual a classe irá se conectar.
		 * @param	Data	$data	Armazena dados que serão reutilizados depois.
		 */
		function run($ano, $periodo) {
			// static::getData($ano, $periodo);
		}

		/**
		 * Pega os dados necessários para a execução da sincronização.
		 * @param	int	$from	Larger than 0
		 * @param	int	@to		Larger than 0
		 */
		function getData($ano, $periodo) {
			// Pegando os dados de inscrições dos professores do Universus:
			$db = new CDB\SyncTeacherEnrolments();
			$from = 1;
			$space = 5000;
			static::$universusTeacherEnrolments = [];
			static::$moodleTeacherEnrolments = Data::getMoodleUserEnrolments('3');

			while (true) {
				$udb->prepare($sql);
				$udb->execute();
				$result = $udb->fetchAll();

				foreach ($result as $enrolment) {
					static::$universusTeacherEnrolments[] = $enrolment;
				}

				static::$log->put('Index requisitado: '.$result['0']['RowNum'].PHP_EOL);
				if (count($result) < $space) {
					break;
				}
				
				$from = $from + $space;
			}

			// Pega apenas os enrolments de professores do Moodle:
			static::$moodleTeacherEnrolments = static::$data->getMoodleUserEnrolments('3', true);
			// Core::echoConsole('Universus Teacher Enrolments', static::$universusTeacherEnrolments);
			// Core::echoConsole('Moodle Teacher Enrolments', static::$moodleTeacherEnrolments);
		}

		/**
		 * Faz a sincronização de enrolments do Universus com o Moodle.
		 */
		public function synchronize(): void {
			$enrolmentstobemade = [];
			$enrolmentsreallymade = [];
			$ignoredenrolments = 0;

			
			// Pulando os que dão enrolment em usuários que não existem:
			$se = &static::$universusTeacherEnrolments;
			$size = count($se);

			$d = static::$data->getMoodleUsers(true);

			for ($i = 0; $i < $size; $i++) {
				$aux = lib::splfixedarray_search($d->username, $se[$i]['LOGIN']);
				if (!$aux) {
					array_splice($se, $i, 1);
					$i--;
					$size--;
				}
			}

			// Selecionando quais enrolments devem ser saltados:
			foreach (static::$universusTeacherEnrolments as $teacherenrolments) {
				$flag = 0;
				$userid_pos = lib::splfixedarray_search($d->username, $teacherenrolments['LOGIN']);
				$userid = $d->id[$userid_pos];
				$shortname = $teacherenrolments['TURMA'].'/'.$teacherenrolments['CODINTERNO_DISC'];
				for ($i = 0; $i < static::$moodleTeacherEnrolments->size; $i++) {
					if ($userid == static::$moodleTeacherEnrolments->userid[$i] && $shortname == static::$moodleTeacherEnrolments->shortname[$i]) {
						$ignoredenrolments++;
						$flag = 1;
						break;
					}
				}
				if ($flag == 0) {
					$enrolmentstobemade[] = $teacherenrolments;
				}
			}

			$size = count($enrolmentstobemade);

			static::$log->putL('Enrolments que serão realizados: '.$size);
			static::$log->putL('Enrolments saltados: '.$ignoredenrolments);

			// Insere os professores no moodle de 50 em 50
			$aux = [];
			$offset = 0;
			foreach ($enrolmentstobemade as $key=>$teacherenrolments) {
				$aux[] = $teacherenrolments;
				if ($offset == 49 || $key == $size-1) {
					// Insere os professores:
					$create_student_enrolments_resp = WSManager::enrol_users($aux, 3, static::$data);

					$aux = [];
					$offset = 0;
				} else {
					$offset++;
				}
			}
		}

		/**
		* Gera uma lista de estudantes que pode ser utilizada para popular o Moodle através 
		* da função core_user_create_users do webservice do Moodle.
		*/
		public function addRandomStudents(int $num) : void {
			for ($i = 0; $i < $num; $i++) {
				$this->universusStudents[] = array(
					'ALUNO' => 'random',
					'SOBRENOME' => lib::random_str(50),
					'MATRICULA' => lib::random_str(20, '1234567890'),
					'CPF' => lib::random_str(11),
					'EMAIL' => lib::random_str(15).'@'.lib::random_str(4).'.com'
				);
			}
		}

		private static $log;
		private static $universusTeacherEnrolments;
		private static $moodleTeacherEnrolments;
	}