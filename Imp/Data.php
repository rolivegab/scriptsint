<?php
	/**
	 * Esta classe armazena dados que serão utilizados ao longo do programa, de forma a utilizá-los novamente depois evitando ter que pegá-los novamente no banco.
	 */
	abstract class Data {
		public static function getMoodleUsers($renew = false) {
			if ($renew === true) {
				static::$students = null;
			}

			if (!static::$students) {
				$db = new CDB\Data();
				$stmt = $db->_moodleUsers();
				$size = $stmt->rowCount();
				static::$students = new stdClass();
				static::$students->id = new SplFixedArray($size);
				static::$students->username = new SplFixedArray($size);
				static::$students->email = new SplFixedArray($size);
				static::$students->size = $size;
				
				$result = $stmt->fetchAll();

				foreach ($result as $key=>$student) {
					static::$students->id[$key] = $student->id;
					static::$students->username[$key] = $student->username;
					static::$students->email[$key] = $student->email;
				}
			}

			return static::$students;
		}

		public static function getMoodleCategories($renew = false) {
			if ($renew === true) {
				static::$categories = null;
			}
			if (!static::$categories) {
				$db = new CDB\Data();
				$stmt = $db->_moodleCategories();

				$size = $stmt->rowCount();

				static::$categories = new stdClass();
				static::$categories->id = new SplFixedArray($size);
				static::$categories->name = new SplFixedArray($size);
				static::$categories->size = $size;

				$result = $stmt->fetchAll();

				foreach ($result as $key=>$category) {
					static::$categories->id[$key] = $category->id;
					static::$categories->name[$key] = $category->name;
				}
			}

			return static::$categories;
		}

		public static function getMoodleCourses($renew = false) {
			if ($renew === true) {
				static::$courses = null;
			}
			if (!static::$courses) {
				$db = new CDB\Data();
				$stmt = $db->_moodleCourses();

				$size = $stmt->rowCount();

				static::$courses = new stdClass();
				static::$courses->id = new SplFixedArray($size);
				static::$courses->shortname = new SplFixedArray($size);
				static::$courses->idnumber = new SplFixedArray($size);
				static::$courses->size = $size;

				$result = $stmt->fetchAll();

				foreach ($result as $key=>$category) {
					static::$courses->id[$key] = $category->id;
					static::$courses->shortname[$key] = $category->shortname;
					static::$courses->idnumber[$key] = $category->idnumber;
				}
			}

			return static::$courses;
		}

		public static function getMoodleUserEnrolments($roleid, $renew = false) {
			if ($renew) {
				static::$userEnrolments = null;
			}
			if (!static::$userEnrolments) {
				$db = new CDB\Data();
				$stmt = $db->_moodleUserEnrolments($roleid);

				$size = $stmt->rowCount();
				static::$userEnrolments = new stdClass;
				static::$userEnrolments->userid = new SplFixedArray($size);
				static::$userEnrolments->username = new SplFixedArray($size);
				static::$userEnrolments->size = $size;

				$result = $stmt->fetchAll();

				foreach ($result as $key=>$userenrolment) {
					static::$userEnrolments->userid[$key] = $userenrolment->userid;
					static::$userEnrolments->shortname[$key] = $userenrolment->shortname;
				}
			}

			return static::$userEnrolments;
		}

		public static function destroyAll() {
			static::$students = null;
			static::$categories = null;
			static::$courses = null;
		}

		private static $students;
		private static $categories;
		private static $courses;
		private static $userEnrolments;
	}