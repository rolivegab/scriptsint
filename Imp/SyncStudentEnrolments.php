<?php
	abstract class SyncStudentEnrolments extends Sync {
		function initialize() {
			static::$log = new Log('syncStudentEnrolments');
		}
		/**
		 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
		 * @param	string	$view	Nome da view a qual a classe irá se conectar.
		 * @param	Data	$data	Armazena dados que serão reutilizados depois.
		 */
		function run($ano, $periodo) {
			static::getData($ano, $periodo);
		}

		/**
		 * Pega os dados necessários para a execução da sincronização.
		 * @param	int	$from	Larger than 0
		 * @param	int	@to		Larger than 0
		 */
		function getData($ano, $periodo) {
			// Pegando os dados de inscrições dos alunos do Universus:
			$db = new CDB\SyncStudentEnrolments();
			$from = 1;
			$space = 5000;
			static::$universusStudentEnrolments = [];
			static::$moodleStudentEnrolments = Data::getMoodleUserEnrolments('5');

			while (true) {
				$stmt = $db->_universusStudentEnrolments($ano, $periodo, $from, $space);

				if ($stmt) {
					$results = $stmt->fetchAll();
					$size = count($results);

					if($size == 0) {
						break;
					}

					array_push(static::$universusStudentEnrolments, ...$results);
				}

				$from = $from + $space;
			}

			// Pega apenas os enrolments de estudantes do Moodle:
			// static::$moodleStudentEnrolments = static::$data->getMoodleUserEnrolments('5');
			// Core::echoConsole('Universus Student Enrolments', static::$universusStudentEnrolments);
			// Core::echoConsole('Moodle Student Enrolments', static::$moodleStudentEnrolments);
			static::synchronize();
		}

		/**
		 * Faz a sincronização de enrolments do Universus com o Moodle.
		 */
		public static function synchronize(): void {
			$enrolmentstobemade = [];
			$enrolmentsreallymade = [];
			$ignoredenrolments = 0;

			
			// Pulando os que dão enrolment em usuários que não existem no moodle:
			$se = &static::$universusStudentEnrolments;
			$size = count($se);

			$d = Data::getMoodleUsers(true);

			for ($i = 0; $i < $size; $i++) {
				$aux = lib::splfixedarray_search($d->username, $se[$i]->matricula);
				if (!$aux) {
					array_splice($se, $i, 1);
					$i--;
					$size--;
				}
			}

			// Selecionando quais enrolments devem ser saltados
			foreach (static::$universusStudentEnrolments as $studentenrolments) {
				$flag = 0;
				$userid_pos = lib::splfixedarray_search($d->username, $studentenrolments->matricula);
				$userid = $d->id[$userid_pos];
				$shortname = $studentenrolments->turma.'/'.$studentenrolments->coddisciplina;
				for ($i = 0; $i < static::$moodleStudentEnrolments->size; $i++) {
					if ($userid == static::$moodleStudentEnrolments->userid[$i] && $shortname == static::$moodleStudentEnrolments->shortname[$i]) {
						$ignoredenrolments++;
						$flag = 1;
						break;
					}
				}
				if ($flag == 0) {
					$enrolmentstobemade[] = $studentenrolments;
				}
			}

			$size = count($enrolmentstobemade);

			static::$log->putL('Enrolments que serão realizados: '.$size);
			static::$log->putL('Enrolments saltados: '.$ignoredenrolments);

			// Insere os alunos no moodle de 50 em 50
			$aux = [];
			$offset = 0;
			foreach ($enrolmentstobemade as $key=>$studentenrolments) {
				$aux[] = $studentenrolments;
				if ($offset == 49 || $key == $size-1) {
					// Insere os alunos:
					$create_student_enrolments_resp = WSManager::enrol_users($aux, 5);

					$aux = [];
					$offset = 0;
				} else {
					$offset++;
				}
			}
		}

		/**
		* Gera uma lista de estudantes que pode ser utilizada para popular o Moodle através 
		* da função core_user_create_users do webservice do Moodle.
		*/
		public function addRandomStudents(int $num): void {
			for ($i = 0; $i < $num; $i++) {
				static::$universusStudents[] = array(
					'ALUNO' => 'random',
					'SOBRENOME' => lib::random_str(50),
					'MATRICULA' => lib::random_str(20, '1234567890'),
					'CPF' => lib::random_str(11),
					'EMAIL' => lib::random_str(15).'@'.lib::random_str(4).'.com'
				);
			}
		}

		private static $log;
		private static $universusStudentEnrolments;
		private static $moodleStudentEnrolments;
	}