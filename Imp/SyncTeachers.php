<?php
abstract class SyncTeachers extends SyncUser
{
	function initialize()
	{
		static::$log = new Log('syncTeachers');
	}
	/**
	 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
	 * @param	string	$view	Nome da view a qual a classe irá se conectar.
	 * @param	Data	$data	Armazena dados que serão reutilizados depois.
	 */
	function run()
	{
		static::$log = new Log('syncTeachers');
		static::getData();
	}

	/**
	 * Pega os dados necessários para a execução da sincronização.
	 */
	function getData()
	{
			// Pegando professores do Universus:
		$udb = new CDB\SyncTeachers();
		$datadb = new CDB\Data();
		$stmt = $udb->_universusTeachers();

		if (!$stmt) {
			static::$log->putL('Erro ao pegar os professores no banco.');
			die();
		}

		static::$universusTeachers = $stmt->fetchAll();
		static::$moodleTeachers = Data::getMoodleUsers();
		// Core::echoConsole('Universus Teachers', static::$universusTeachers);
		// Core::echoConsole('Moodle Teachers', static::$moodleTeachers);
		static::checkData();
	}

	/**
	 * Checa se existe algum campo incorreto, ou duplicado:
	 */
	public function checkData()
	{
		$t = &static::$universusTeachers;
		$l = static::$log;

		$size = count($t);
		$erros = 0;
		for ($i = 0; $i < $size; $i++) {
			// Converte username para minúsculo
			$t[$i]->login = strtolower($t[$i]->login);

			// Checa se o username está no formato correto
			// ENVIAR EMAIL DEPOIS!
			if ($t[$i]->login == '') {
				$l->putL('Um usuário com username vazio foi ignorado!');
				array_splice($t, $i, 1);
				$size--;
				$i--;
				$erros++;
				continue;
			}
			if (!static::checkUsername($t[$i]->login, 'az', '09', '_', '-', '.', '@')) {
				$l->putL('Usuário \'' . $t[$i]->login . '\' ignorado por conter caracteres inválidos no campo de usuário.');
				array_splice($t, $i, 1);
				$size--;
				$i--;
				$erros++;
				continue;
			}

			// Checa se o email está no formato correto
			// ENVIAR EMAIL DEPOIS!
			if (!filter_var($t[$i]->email, FILTER_VALIDATE_EMAIL)) {
				$l->putL('Usuário \'' . $t[$i]->login . '\' ignorado por conter caracteres inválidos no campo de email. Email: \'' . $t[$i]->email . '\'.');
				array_splice($t, $i, 1);
				$size--;
				$i--;
				$erros++;
				continue;
			}

			// Checa se há campos únicos duplicados:
			for ($j = $i + 1; $j < $size; $j++) {
				// ENVIAR EMAIL PARA O SEGUNDO USUÁRIO!
				if ($t[$i]->login == $t[$j]->login) {
					$l->putL('Usuário duplicado: \'' . $t[$i]->login . '\'.');
					$l->putL('Apenas o primeiro será adicionado.');
					array_splice($t, $j, 1);
					$size--;
					$j--;
					continue;
					$erros++;
				}

				// ENVIAR EMAIL DEPOIS PARA O SEGUNDO USUÁRIO!
				if ($t[$i]->email == $t[$j]->email) {
					$l->putL('Email duplicado entre usuários: \'' . $t[$i]->login . '\' e \'' . $t[$j]->login . '\'. Email: \'' . $t[$i]->email . '\'.');
					$l->putL('Apenas o usuário \'' . $t[$i]->login . '\' será adicionado.');
					array_splice($t, $j, 1);
					$size--;
					$j--;
					$erros++;
					continue;
				}
			}
		}

		$l->putL('Usuários do universus com ERRO: ' . $erros);
		static::synchronize();
	}

	/**
	 * Faz a sincronização de professores do Universus com o Moodle.
	 */
	public function synchronize() : void
	{
		$userstobeinserted = [];
		$usersreallyinserted = [];
		$ignoredusers = 0;

		// Selecionando quais usuários devem ser saltados
		foreach (static::$universusTeachers as $teacher) {
			$flag = 0;
			for ($i = 0; $i < static::$moodleTeachers->size; $i++) {
				if (strtolower($teacher->login) == strtolower(static::$moodleTeachers->username[$i]) || strtolower($teacher->email) == strtolower(static::$moodleTeachers->email[$i])) {
					$ignoredusers++;
					$flag = 1;
					break;
				}
			}
			if ($flag == 0) {
				$userstobeinserted[] = $teacher;
			}
		}

		$size = count($userstobeinserted);

		static::$log->putL('Professores que serão inseridos: ' . $size);
		static::$log->putL('Professores saltados: ' . $ignoredusers);

		// Insere os professores no moodle de 50 em 50
		$aux = [];
		$offset = 0;
		foreach ($userstobeinserted as $key => $user) {
			$aux[] = $user;
			if ($offset == 49 || $key == $size - 1) {
				for ($i = 0; $i < 3; $i++) {
					$create_users_resp = WSManager::create_users($aux, 'teacher');
					if ($create_users_resp) {
						foreach ($create_users_resp as $resp) {
							$usersreallyinserted[] = $resp;
						}

						break;
					} else {
						static::$log->putL('Falha ao executar create_users, tentativa ' . $i);
					}
				}

				$aux = [];
				$offset = 0;
			} else {
				$offset++;
			}
		}

		static::$log->putL('Número de usuários realmente inseridos: ' . count($usersreallyinserted));
		static::$log->putL('IDs inseridos: ');
		foreach ($usersreallyinserted as $user) {
			static::$log->putL($user->id . ' ' . $user->username);
		}
	}

	private static $log;
	private static $view;
	private static $universusTeachers;
	private static $moodleTeachers;
}