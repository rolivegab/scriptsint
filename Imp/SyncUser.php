<?php
	require_once(__DIR__.'/Sync.php');

	abstract class SyncUser extends Sync {
        /**
         * Checa se o username passado possui apenas os caracteres definidos nos argumentos seguintes.
         * @param	$username	Username a ser verificado.
         * @param	$arg,...	Caracteres permitidos no username, pode ser apenas 1 caractere ou 2. 
		 * Se for 2, é definido um intervalo. 
		 * EX: 'az' indica todos os caracteres de 'a' a 'z'. 
		 * O segundo deve ser sempre maior que o primeiro.
         */
        public static function checkUsername(string $username): bool {
			$separators = [];
			$args = array_slice(func_get_args(), 1);

			// Check args:
			foreach ($args as $arg) {
				// Check if is string
				if (!is_string($arg)) {
					return false;
				}
				
				$size = strlen($arg);

				// Check if arg is valid:
				if ($size > 2 || $size == 2 && $arg[0] > $arg[1]) {
					return false;
				}

				// Insert arg into separators:
				if ($size == 1) {
					$separators[] = ord($arg[0]);
				} else if ($size == 2) {
					$from = ord($arg[0]);
					$to = ord($arg[1]);
					for ($i = $from; $i <= $to; $i++) {
						$separators[] = $i;
					}
				}
			}

			// Check if every character of username are in separators.
			$size = strlen($username);
			for ($i = 0; $i < $size; $i++) {
				$flag = false;
				foreach ($separators as $s) {
					if (ord($username[$i]) == $s) {
						$flag = true;
					}
				}
				if ($flag == false) {
					return false;
				}
			}

			return true;
        }
	}