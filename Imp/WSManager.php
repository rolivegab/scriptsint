<?php
	abstract class WSManager {
		public static function initialize() {
			global $config;

			// Add last slash if user don't place on config.php
			static::$moodleurl = $config->WS->moodleurl.'/';
			static::$token = $config->WS->token;
			static::$function = new stdClass();
			static::initializeFunctions();
			static::$log = new Log('WSManager');
		}

		private static function initializeFunctions(): void {
			static::addWSFunction('create_users', 'core_user_create_users');
			static::addWSFunction('get_users', 'core_user_get_users');
			static::addWSFunction('get_users_by_field', 'core_user_get_users_by_field');
			static::addWSFunction('create_categories', 'core_course_create_categories');
			static::addWSFunction('create_courses', 'core_course_create_courses');
			static::addWSFunction('enrol_users', 'enrol_manual_enrol_users');
		}

		public static function get_users_by_field($field): bool {
			$json = $this->curlRequest($this->function->get_users_by_field, $field);
			
			return false;
		}

		public static function get_users($array, $criterias): int {
			$json = json_decode($this->curlRequest($this->function->get_users, $criterias));

			return false;
		}

		/**
		 * Insere um array de usuários, e retorna um array com ids e usernames caso funcione, e null caso não funcione.
		 */
		public static function create_users($array, $role): ?array {
			if ($role == 'student') {
				$parameters = [];

				foreach($array as $key=>$user) {
					$parameters["users[$key][username]"] = $user->matricula;
					$parameters["users[$key][password]"] = 'facex123';
					$parameters["users[$key][firstname]"] = lib::getFirstName($user->aluno);
					$parameters["users[$key][lastname]"] = lib::getLastName($user->aluno, $user->sobrenome);
					$parameters["users[$key][email]"] = $user->email;
				}
			} else if ($role == 'teacher') {
				$parameters = [];

				foreach ($array as $key=>$user) {
					$parameters["users[$key][username]"] = $user->login;
					$parameters["users[$key][password]"] = 'facex123';
					$parameters["users[$key][firstname]"] = lib::getFirstName($user->nome);
					$parameters["users[$key][lastname]"] = lib::getLastName($user->nome, $user->sobrenome);
					$parameters["users[$key][email]"] = $user->email;
				}
			}

			return static::parseResp( static::curlRequest(static::$function->create_users, $parameters) );
		}

		public static function parseResp(string $resp): ?array {
			$json = json_decode($resp);

			if ($json) {
				if (is_object($json) && property_exists($json, 'exception')) {
					static::$log->putL('Exception: '.$json->exception);
					static::$log->putL('Error Code: '.$json->errorcode);
					static::$log->putL('Message: '.$json->message);
					if (property_exists($json, 'debuginfo')) {
						static::$log->putL('Debug Info: '.$json->debuginfo);
					} else {
						static::$log->putL('Debug info disabled.');
					}

					static::$log->close();

					return null;
				}
			} else {
				static::$log->putL($json);
			}

			return $json;
		}

		public static function create_categories($array) {
			$parameters = [];

			foreach ($array as $key=>$course) {
				$parameters["categories[$key][name]"] = $course->curso;
			}
			
			return static::parseResp( static::curlRequest(static::$function->create_categories, $parameters) );
		}

		public static function create_courses($array) {
			$parameters = [];
			$c = Data::getMoodleCategories(true);

			foreach ($array as $key=>$course) {
				$parameters["courses[$key][fullname]"] = $course->disciplina;
				$parameters["courses[$key][shortname]"] = $course->turma.'/'.$course->coddisciplina;
				$aux = lib::splfixedarray_search($c->name, $course->curso);
				if ($aux) {
					$parameters["courses[$key][categoryid]"] = $c->id[$aux];
				} else {
					// Indicar que a curso X não foi adicionada pois a categoria Y não existia no Moodle.
					return null;
				}
			}

			return static::parseResp( static::curlRequest(static::$function->create_courses, $parameters));
		}

		public static function enrol_users($array, $role) {
			$parameters = [];
			$u = Data::getMoodleUsers();
			$c = Data::getMoodleCourses(true);

			if ($role == 5) {
				foreach ($array as $key=>$studentenrolments) {
					$parameters['enrolments['.$key.'][roleid]'] = $role;

					$aux = lib::splfixedarray_search( $u->username, $studentenrolments->matricula);
					if ($aux) {
						$parameters['enrolments['.$key.'][userid]'] = $u->id[$aux];
					} else {
						return null;
					}

					$aux = lib::splfixedarray_search( $c->shortname, $studentenrolments->turma.'/'.$studentenrolments->coddisciplina);
					if ($aux) {
						$parameters['enrolments['.$key.'][courseid]'] = $c->id[$aux];
					} else {
						return null;
					}
				}

				return static::parseResp( static::curlRequest(static::$function->enrol_users, $parameters) );

			} else if ($role == 3) {
				foreach ($array as $key=>$teacherenrolments) {
					$parameters['enrolments['.$key.'][roleid]'] = $role;

					$aux = lib::splfixedarray_search( $u->username, $teacherenrolments->login);
					if($aux) {
						$parameters['enrolments['.$key.'][userid]'] = $u->id[$aux];
					} else {
						return null;
					}

					$aux = lib::splfixedarray_search( $c->shortname, $teacherenrolments->turma.'/'.$teacherenrolments->codinterno_disc);
					if ($aux) {
						$parameters['enrolments['.$key.'][courseid]'] = $c->id[$aux];
					} else {
						return null;
					}
				}

				return $this->parseResp( $this->curlRequest($this->function->enrol_users, $parameters) );
			}
		}

		public static function curlRequest(stdClass $function, array $postparameters): string {
			$headers = array (
				'wstoken' => static::$token,
				'wsfunction' => $function->wsfunction,
				'moodlewsrestformat' => 'json'
			);

			$postfields = array_merge($headers, $postparameters);

			$curl = curl_init();

			curl_setopt_array($curl, 
				array (
					CURLOPT_RETURNTRANSFER => 1,
					CURLOPT_URL => $function->url,
					CURLOPT_POST => 1,
					CURLOPT_USERAGENT => $_SERVER ['HTTP_USER_AGENT'],
					CURLOPT_SSL_VERIFYPEER => true,
					CURLOPT_CAINFO => __DIR__ . "/cacert.pem",
					CURLOPT_POSTFIELDS => $postfields
				)
			);

			$resp = curl_exec($curl);
			$curl = curl_close($curl);

			return $resp;
		}

		private static function addWSFunction($name, $wsfunction): bool {
			$f = static::$function;

			if (isset(static::$name)) {
				return false;
			}

			$f->$name = new stdClass();
			$f->$name->wsfunction = $wsfunction;
			$f->$name->url = static::$moodleurl.'webservice/rest/server.php';

			return true;
		}

		private static $token;
		private static $function;
		private static $log;
		private static $moodleurl;
	}

	WSManager::initialize();