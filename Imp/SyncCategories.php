<?php
    abstract class SyncCategories extends Sync {
		public static function initialize() {
			static::$log = new Log('syncCategories');
		}
        /**
		 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
		 * @param	string	$view	Nome da view a qual a classe irá se conectar.
		 * @param	Data	$data	Armazena dados que serão reutilizados depois.
		 */
        public static function run($ano, $periodo) {
			static::getData($ano, $periodo);
        }

        /**
         * Pega os dados necessários para a execução da sincronização.
         */
        public function getData($ano, $periodo) {
			$udb = new CDB\SyncCategories();

            // Pegando categorias do universus:
			$stmt = $udb->_universusCategories($ano, $periodo);
			if (!$stmt) {
				static::$log->putL('Erro ao pegar categorias no banco.');
				die();
			}

			static::$universusCategories = $stmt->fetchAll();
			static::$moodleCategories = Data::getMoodleCategories();
			// Core::echoConsole('Universus Categories', static::$universusCategories);
			// Core::echoConsole('Moodle Categories', static::$moodleCategories);
			static::checkData();
        }

        /**
		 * Checa se existe algum campo incorreto, ou duplicado:
		 */
		public function checkData() {
            $c = &static::$universusCategories;
            $l = static::$log;
            
			$size = count($c);
			$erros = 0;
            for ($i = 0; $i < $size; $i++) {
				// Checa se a categoria está vazia:
				if ($c[$i]->curso == '') {
					$l->putL('Uma categoria vazia foi ignorada!');
					array_splice($c, $i, 1);
					$size--;
					$i--;
					$erros++;
					continue;
				}
			}
			
			$l->putL('Categorias do Universus com ERRO: '.$erros);
			static::synchronize();
		}

		public static function synchronize() : void {
			$categoriestobeinserted = [];
			$categoriesreallyinserted = [];
			$ignoredcategories = 0;

			// Selecionando quais categorias devem ser saltadas
			foreach (static::$universusCategories as $category) {
				$flag = 0;
				for ($i = 0; $i < static::$moodleCategories->size; $i++) {
					if ($category->curso == static::$moodleCategories->name[$i]) {
						$ignoredcategories++;
						$flag = 1;
						break;
					}
				}
				if ($flag == 0) {
					$categoriestobeinserted[] = $category;
				}
			}

			$size = count($categoriestobeinserted);

			static::$log->putL('Categorias que serão inseridos: '.$size);
			static::$log->putL('Categorias saltadas: '.$ignoredcategories);

			// Insere as categorias no moodle de 10 em 10:
			$aux = [];
			$offset = 0;
			foreach ($categoriestobeinserted as $key=>$category) {
				$aux[] = $category;
				if ($offset == 9 || $key == $size-1) {
					for ($i = 0; $i < 3; $i++) {
						$create_categories = WSManager::create_categories($aux);
						if ($create_categories) {
							foreach ($create_categories as $resp) {
								$categoriesreallyinserted[] = $resp;
							}

							break;
						} else {
							static::$log->putL('Falha ao executar create_categories, tentativa '.$i);
						}
					}

					$aux = [];
					$offset = 0;
				} else  {
					$offset++;
				}
			}

			static::$log->putL('Número de categorias realmente inseridas: '.count($categoriesreallyinserted));
			static::$log->putL('IDs inseridos: ');
			foreach ($categoriesreallyinserted as $category) {
				static::$log->put($category->id.' '.$category->name);
			}
		}

		private static $log;
        private static $moodleCategories;
		private static $universusCategories;
    }