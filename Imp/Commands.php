<?php
	require_once(__DIR__.'/Sync.php');
    require_once( realpath(__DIR__.'/../Imp/lib.php') );

	class Commands extends Sync {
		function __construct($data, $config) {
            static::$config = $config;
            // parent::__construct($data);
			// $this->log = new Log('Commands');
            // static::$data = $data;
		}

        /**
        * Pesquisa em cursos nos campos: DISCIPLINA, CURSO, CODDISCIPLINA e TURMA.
        * @param array Array de parâmetros.
        */
        public static function searchCourses(array $params) {
            $udb = static::$server->universusConnDB();
            $view = static::$config->disciplina;

            $sql = "SELECT * FROM $view";
            if ($params) {
                $first = true;
                foreach ($params as $key=>$param) {
                    if ($first) {
                        $sql .= ' WHERE '.$key.' LIKE \'%'.$param.'%\'';
                        $first == false;
                    } else {
                        $sql .= ' AND '.$key.' LIKE \'%'.$param.'%\'';
                    }
                }
            }

            $udb->prepare($sql);
            $udb->execute($sql);
            $result = $udb->fetchAll();

            return $result;
        }

        /**
        * Lista os estudantes daquele curso em específico.
        * @param string $coddisciplina Código da disciplina.
        * @param string $turma Código da turma.
        * @param string $course Nome do curso.
        */
        public static function listStudentsOfCourse($coddisciplina, $turma, $course = NULL) {
            $udb = static::$server->universusConnDB();
            $papelaluno = static::$config->papelaluno;
            $aluno = static::$config->aluno;

            $sql = "SELECT * FROM 
            $aluno LEFT JOIN $papelaluno ON ($aluno.MATRICULA = $papelaluno.MATRICULA) 
            WHERE CODDISCIPLINA = '$coddisciplina' AND TURMA = '$turma' ORDER BY $aluno.ALUNO";
            $udb->prepare($sql);
            $udb->execute($sql);
            $result = $udb->fetchAll();

            echo 'username;email;role1;firstname;lastname'.($course ? ';course1' : '').'&#13;&#10';
            foreach ($result as $aluno) {
                echo mb_strtolower($aluno['EMAIL'], 'UTF-8').';'.
                mb_strtolower($aluno['EMAIL'], 'UTF-8').';'.
                'student'.';'.
                lib::getFirstName($aluno['ALUNO']).';'.
                lib::getLastName($aluno['ALUNO'], $aluno['SOBRENOME']).
                ($course ? ';'.$course : '').'&#13;&#10';
            }
        }

        public static function searchStudentClasses($matricula) {
            $udb = static::$server->universusConnDB();
            $disciplina = static::$config->disciplina;
            $papelaluno = static::$config->papelaluno;
            $aluno = static::$config->aluno;

            $sql = "
                SELECT 
                $disciplina.CODDISCIPLINA,
                $disciplina.TURMA,
                $disciplina.DISCIPLINA
                FROM $disciplina 
                LEFT JOIN $papelaluno ON ($disciplina.TURMA = $papelaluno.TURMA 
                    AND $disciplina.CODDISCIPLINA = $papelaluno.CODDISCIPLINA)
                LEFT JOIN $aluno ON ($aluno.MATRICULA = $papelaluno.MATRICULA)
                WHERE $aluno.MATRICULA = '$matricula'
            ";
            $udb->prepare($sql);
            $udb->execute($sql);
            $result = $udb->fetchAll();

            return $result;
        }

        public static function searchMatricula($name) {
            $udb = static::$server->universusConnDB();
            $aluno = static::$config->aluno;

            $sql = "SELECT * FROM $aluno WHERE $aluno.ALUNO LIKE '%$name%'";
            $udb->prepare($sql);
            $udb->execute($sql);
            $result = $udb->fetchAll();

            return $result;
        }

        public static function searchStudent($matricula) {
            $udb = static::$server->universusConnDB();
            $aluno = static::$config->aluno;

            $sql = "SELECT * FROM $aluno WHERE $aluno.MATRICULA = '$matricula'";
            $udb->prepare($sql);
            $udb->execute($sql);
            $result = $udb->fetchAll();

            return $result;
        }

		private $view;
        private static $config;
    }