<?php
	/**
	 * Classe com métodos necessários e outros auxiliares para o funcionamento do sistema.
	 */
	class lib 
	{
		/**
		* Gerar uma string aleatória com um tamanho e caracteres definidos.
		*/
		public static function random_str (int $length, string $keyspace = self::keyspace) : string
		{
		    $str = '';
		    $max = mb_strlen($keyspace, '8bit') - 1;
		    for ($i = 0; $i < $length; ++$i) 
		    {
		        $str .= $keyspace[random_int(0, $max)];
		    }

		    return $str;
		}

		/**
		 * Mostra as views disponíveis no banco de dados do Universus que possuam o sulfixo "_EAD".
		 */
		public static function echoUniversusDatabases() : void
		{
			$udb = cmd::$server->UniversusConnDB();

			$sql = "SELECT name FROM sys.views WHERE name LIKE '%_EAD'";
			$udb->prepare($sql);
			$udb->execute();
			$result = $udb->fetchAll();

			lib::echoConsole($result);
		}

		/**
		* Auxilia na criação de critérios para o comando core_user_get_users do web service do Moodle.
		*/
		public static function addCriteria (string $key, string $value, array &$criterias)
		{
			$size = count($criterias) / 2;
			$criterias["criteria[$size][key]"] = $key;
			$criterias["criteria[$size][value]"] = $value;
		}

		public static function getFirstName(string $name) : string
		{
			$firstname = explode(' ', $name)[0];
			$firstname = explode('/', $firstname)[0];

			return $firstname;
		}

		public static function getLastName(string $firstname, ?string $lastname) : string
		{
			$result = '';
			// Para o caso de o sobrenome ser nulo:
			if($lastname == null)
			{
				$separatednames = explode(' ', $firstname);
				if(count($separatednames) == 1)
				{
					$separatednames = explode('/', $firstname);
					if(count($separatednames) == 1)
					{
						$lastname = '?';
						return $lastname;
					}
				}
				foreach($separatednames as $key=>$name)
				{
					if($key == 1)
					{
						$result .= $name;
					}
					else if($key > 1)
					{
						$result .= ' '.$name;
					}
				}
			}
			else
			{
				$result = $lastname;
			}

			if($result == '')
			{
				echo "ERRO no nome '$firstname' '$lastname'<br>";
			}
			return $result;
		}

		public static function getCourseIDbyShortnameAndIDNumber($shortname, $idnumber)
		{
			require_once('commands.php');

			$mdb = cmd::$server->MoodleConnDB();

			$sql = "SELECT id FROM mdl_course WHERE mdl_course.shortname = '$shortname' AND mdl_course.shortname = '$idnumber'";
			$mdb->prepare($sql);
			$mdb->execute($sql);
			$result = $mdb->fetch();

			return $result['id'];
		}

		/**
		 * Pesquisa valores em splfixedarrays com a condição de que a pesquisa deve achar o mesmo index para todas as colunas.
		 * Para cada array pesquisado, deve ser colocado um valor a ser encontrado.
		 * O número de argumentos deve ser par, e colunas e valores são passados por referência alternadamente, começando pela coluna e em seguida pelo seu valor.
		 * @return o resultado da pesquisa, caso todos os valores sejam encontrados em suas respectivas colunas sob o mesmo index, e null caso não seja possível.
		 */
		public static function splfixedarray_search()
		{
			// O número de argumentos deve ser par, do contrário, retorna null.
			$numargs = func_num_args();
			if($numargs % 2 != 0)
			{
				return null;
			}

			$args = func_get_args();

			// Percorre os argumentos, salvando as colunas e o valor que deve ser encontrado nelas em variáveis para poder realizar a pesquisa logo em seguida.
			for($i = 0; $i < count($args[0]); $i++)
			{
				if($args[0][$i] == $args[1])
				{
					$flag = true;
					for($j = 2; $j < count($args);$j=$j+2)
					{
						if($args[$j][$i] != $args[$j+1])
						{
							$flag = false;
							break;
						}
					}
					if($flag)
					{
						return $i;
					}
				}
			}

			return null;
		}

		// Conjunto de símbolos padrão para a geração de strings aleatórias na função addRandomStudents:
		private const keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	}