<?php
	abstract class SyncCourses extends Sync {
		function initialize() {
			static::$log = new Log('syncCourses');
		}
		/**
		 * Recebe o nome da view com a qual deve se conectar, e a classe que contém as informações armazenadas.
		 * @param	string	$view	Nome da view a qual a classe irá se conectar.
		 * @param	Data	$data	Armazena dados que serão reutilizados depois.
		 */
		function run($ano, $periodo) {
			static::getData($ano, $periodo);
		}

		/**
		 * Pega os dados necessários para a execução da sincronização.
		 */
		function getData($ano, $periodo) {
			$udb = new CDB\SyncCourses();

			$stmt = $udb->_universusCourses($ano, $periodo);

			if (!$stmt) {
				static::$log->putL('Erro ao pegar os cursos do banco.');
				die();
			}

			static::$universusCourses = $stmt->fetchAll();
			static::$moodleCourses = Data::getMoodleCourses();
			// Core::echoConsole('Universus Courses', static::$universusCourses);
			// Core::echoConsole('Moodle Courses', static::$moodleCourses);
			static::checkData();
		}

		/**
		 * Checa se existe algum campo incorreto, ou duplicado:
		 */
		public static function checkData() {
            $c = &static::$universusCourses;
            $l = static::$log;
            
			$size = count($c);
			$erros = 0;
			// Checa se há campos únicos duplicados:
            for ($i = 0; $i < $size; $i++) {
                for ($j = $i+1; $j < $size; $j++) {
					if ($c[$i]->turma.'/'.$c[$i]->coddisciplina == $c[$j]->turma.'/'.$c[$j]->coddisciplina) {
						$l->putL('Curso duplicado: \''.$c[$i]->turma.'/'.$c[$i]->coddisciplina.'\'.');
						$l->putL('Apenas o primeiro será adicionado.');
						array_splice($c, $j, 1);
						$size--;
						$j--;
						$erros++;
						continue;
					}
                }
			}
			
			$l->putL('Cursos do Universus com ERRO: '.$erros);
			static::synchronize();
		}

		/**
		 * Faz a sincronização de professores do Universus com o Moodle.
		 */
		public static function synchronize(): void {
			$coursestobeinserted = [];
			$coursesreallyinserted = [];
			$ignoredcourses = 0;

			// Selecionando quais usuários devem ser saltados
			foreach (static::$universusCourses as $course) {
				$flag = 0;
				for ($i = 0; $i < static::$moodleCourses->size; $i++) {
					if ($course->turma.'/'.$course->coddisciplina == static::$moodleCourses->shortname[$i]) {
						$ignoredcourses++;
						$flag = 1;
						break;
					}
				}
				if ($flag == 0) {
					$coursestobeinserted[] = $course;
				}
			}

			$size = count($coursestobeinserted);

			static::$log->putL('Cursos que serão inseridos: '.$size);
			static::$log->putL('Cursos saltados: '.$ignoredcourses);

			// Insere os cursos no moodle de 50 em 50
			$aux = [];
			$offset = 0;
			foreach ($coursestobeinserted as $key=>$course) {
				$aux[] = $course;
				if ($offset == 49 || $key == $size-1) {
					for ($i = 0; $i < 3; $i++) {
						$create_courses_resp = WSManager::create_courses($aux);
						if ($create_courses_resp) {
							foreach ($create_courses_resp as $resp) {
								$coursesreallyinserted[] = $resp;
							}

							break;
						} else {
							static::$log->putL('Falha ao executar create_courses, tentativa '.$i);
						}
					}

					$aux = [];
					$offset = 0;
				} else  {
					$offset++;
				}
			}

			static::$log->putL('Número de cursos realmente inseridos: '.count($coursesreallyinserted));
			static::$log->putL('IDs inseridos: ');
			foreach ($coursesreallyinserted as $course) {
				static::$log->putL($course->id.' '.$course->shortname);
			}
		}

		private static $log;
		private static $universusCourses;
        private static $moodleCourses;
	}