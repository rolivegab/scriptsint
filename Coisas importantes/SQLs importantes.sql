-- Vê a definição de uma view:
SELECT 
    definition
FROM sys.objects o 
JOIN sys.sql_modules m ON (m.object_id = o.object_id) 
WHERE 
    o.object_id = object_id('AVA_ListaDisciplina_EAD') 
    AND o.type = 'V'

-- Vê as colunas de uma tabela:
SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = N'Customers'

-- Outra versão
SELECT * 
FROM sys.columns 
WHERE object_id = OBJECT_ID('ALUNO') 

-- Para conectar ao universus:
-- $ tsql -H 10.150.20.24 -p 1433 -D universus -U sa -P Voltron01